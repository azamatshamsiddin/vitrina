const GoodsItem = (props) => {
  const {id, name, description, full_background, price, addToCart} = props
  return (
    <div className="card" id={id}>
      <div className="card-image">
        <img src={full_background} alt={name}/>
      </div>
      <div className="card-content">
        <span className="card-title">{name}</span>
        <p>{description}</p>
      </div>
      <div className="card-action">
        <button onClick={() => addToCart({id, name, description, full_background, price})} className="btn">Buy</button>
        <span className="right">$ {price}</span>
      </div>
    </div>
  )
}

export default GoodsItem
