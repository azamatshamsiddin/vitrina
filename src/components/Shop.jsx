import { useEffect, useState } from "react";
import Loader from './Loader'
import { API_KEY, API_URL } from "../config";
import GoodsList from "./GoodsList";
import Basket from "./Basket";
import BasketList from "./BasketList";

const Shop = () => {
  
  const [goods, setGoods] = useState([])
  const [loading, setLoading] = useState(true)
  const [order, setOrder] = useState([])
  const [isBasketShow, setBasketShow] = useState(false)
  
  useEffect(() => {
    fetch(API_URL, {
      headers: {
        "Authorization": API_KEY
      }
    }).then(res => res.json())
      .then(data => {
        
        data.featured && setGoods(data.featured)
        console.log(data.featured)
        setLoading(false)
      })
  }, [])
  
  const handleBasketShow = () => {
    setBasketShow(!isBasketShow)
  }
  
  const removeBasketItem = (itemID) => {
    const newOrder = order.filter(item => item.id !== itemID)
    setOrder(newOrder)
  }
  
  
  const addToCart = (item) => {
    const itemIndex = order.findIndex(orderItem => orderItem.id === item.id);
    if (itemIndex < 0) {
      const newItem = {
        ...item,
        quantity: 1
      };
      setOrder([...order, newItem])
    } else {
      const newOrder = order.map((orderItem, index) => {
        if (index === itemIndex) {
          return {
            ...orderItem,
            quantity: orderItem.quantity + 1
          }
        } else {
          return orderItem
        }
      });
      setOrder(newOrder)
    }
  }
  
  const incrementQuantity = (itemId) => {
    const newOrder = order.map(item => {
      if (item.id === itemId) {
        const newQuantity = item.quantity + 1
        return {
          ...item,
          quantity: newQuantity
        }
      } else {
        return item
      }
    });
    setOrder(newOrder)
  }
  const decrementQuantity = (itemId) => {
    const newOrder = order.map(item => {
      if (item.id === itemId) {
        const newQuantity = item.quantity - 1
        return {
          ...item,
          quantity: newQuantity >= 0 ? newQuantity : 0
        }
      }else {
        return item
      }
    });
    setOrder(newOrder)
  }
  
  return (
    <div className="content">
      <Basket quantity={order.length} handleBasketShow={handleBasketShow}/>
      {
        loading ? <Loader/> : <GoodsList addToCart={addToCart} goods={goods}/>
      }
      {isBasketShow &&
        <BasketList decrementQuantity={decrementQuantity} incrementQuantity={incrementQuantity}
                    removeBasketItem={removeBasketItem} order={order} handleBasketShow={handleBasketShow}/>}
    </div>
  )
}

export default Shop