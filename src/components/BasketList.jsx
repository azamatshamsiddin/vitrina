import BasketItem from "./BasketItem";

const BasketList = (props) => {
  const {order, handleBasketShow,removeBasketItem,incrementQuantity, decrementQuantity} = props
  
  const totalPrice = order.reduce((sum,el)=>{
    return sum + el.price *el.quantity
  },0)
  
  return (
    <div className="bsk">
      <ul className="collection basket-list">
        <li className="collection-item active">Basket</li>
        {
          order.length ? order.map(item => (
            <BasketItem incrementQuantity={incrementQuantity} decrementQuantity={decrementQuantity} removeBasketItem={removeBasketItem} key={item.id} {...item}/>
          )) : <li className="collection-item">Basket is empty</li>
        }
        <li className="collection-item active">
          Total cost: ${totalPrice}
        </li>
        <i className="material-icons basket-close" onClick={handleBasketShow}>close</i>
  
      </ul>
    </div>
    
  )
}

export default BasketList