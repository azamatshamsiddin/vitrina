import GoodsItem from "./GoodsItem";

const GoodsList = (props)=>{
  const {goods = [], addToCart} = props
  if(!goods.length){
    return <h1>Nothing here</h1>
  }
  return (
    <div className="goods">
       {
         goods.map(item=>(
           <GoodsItem addToCart={addToCart} key={item.id} {...item}/>
         ))
       }
    </div>

  )
}

export default GoodsList

