const BasketItem = (props) => {
  const {id, price, name, quantity, removeBasketItem, incrementQuantity, decrementQuantity} = props
  return (
    <li className="collection-item">{name} x{quantity} = ${price * quantity}
      <button className="waves-effect waves-light btn" onClick={()=>incrementQuantity(id)}><i className="material-icons">exposure_plus_1</i></button>
      <span className="secondary-content">
        <i className="material-icons basket-delete" onClick={() => removeBasketItem(id)}>delete_forever</i>
      </span>
      <button className="waves-effect waves-light btn" style={{marginLeft: "10px"}} onClick={()=>decrementQuantity(id)}><i className="material-icons">exposure_minus_1</i></button>

    </li>)
}

export default BasketItem