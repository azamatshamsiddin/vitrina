import Header from "./components/Header";
import Footer from "./components/Footer";
import Shop from "./components/Shop";

function App() {
  return (
    <div>
      <Header/>
      <main className='container'>
        <Shop/>
      </main>
      <Footer/>
    </div>
  );
}

export default App;
